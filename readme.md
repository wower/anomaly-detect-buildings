# Anomaly Detection in Buildings with Machine Learning

<img src=images/readme-sm-titleplate.png width='700'>

This project progresses through several notebooks each presenting different aspects of time series analysis and anomaly detection (also called outlier detection).   

## Project Overview

|           Notebook                        |      Description                  | 
| ----------------------------------------- |        --------------------------- |
|`outlier-detection-demo.ipynb`             | Demonstrates basic outlier detect methods | 
|`unsupervised-time-series-methods.ipynb`   | Contains examples of unsupervised ML algorithms for detecting outliers in building data | 
|`supervised-time-series-setup.ipynb`       | Code to pre-process building data and create models for time series prediction |  
|`supervised-time-series-methods.ipynb`     | Demonstrates 2 supervised ML methods to detect anomalies in building data | 
|`util.py`     | Data loader and Dataclasses | 

## Building Time Series Analysis
There are three reasons why accurate anomaly detection in building data is important:

|Proactive Maintenance| Energy Efficiency | Occupant Comfort and Safety  |
|-------|------|-----|
|<img src="images/readme-Building Predictive Icons-01.png" width="300" />|<img src="images/readme-Building Predictive Icons-02.png" width="300"/>|<img src="images/readme-Building Predictive Icons-03.png" width="300"/>|

1. Proactive Maintenance: Accurate anomaly detection in building time series data enables early identification of equipment malfunctions or inefficiencies, allowing for proactive maintenance actions. This helps minimize downtime, reduce repair costs, and extend equipment lifespan.
2. Energy Efficiency: Anomaly detection pinpoints energy consumption anomalies, facilitating targeted interventions to optimize building energy efficiency. By identifying and addressing energy wastage, buildings can cut operational costs, lower carbon emissions, and enhance sustainability efforts.
3. Occupant Comfort and Safety: Detecting anomalies in environmental conditions like temperature or air quality ensures prompt actions to maintain comfortable and safe indoor environments. This contributes to improved tenant satisfaction, reduced complaints, and healthier indoor spaces, enhancing overall occupant experience.

## 2D Demonstration of Outlier Detection Methods
To gain insight into various outlier detection methodologies, the following summaries describe 8 distinct algorithms and how they work. In the chart which follows, particular attention is to be paid to Isolation Trees and K Nearest Neighbors (KNN) techniques, as evidenced by their robust performance.

- **Angle-based Outlier Detector (ABOD)**: ABOD measures the variance in angles formed by data points with respect to others, identifying outliers with larger variations. It's computationally efficient and effective in high-dimensional data but sensitive to outliers and requires tuning.
- **Gaussian Mixture Model (GMM)**: GMM assumes data is generated from a mixture of several Gaussian distributions. It estimates parameters such as means and covariances to identify outliers as data points with low probability densities.
- **Isolation Forest**: Isolation Forest isolates outliers by randomly partitioning data into subsets. It identifies anomalies as points requiring fewer partitions to isolate, making it efficient for large datasets with high-dimensional features and resistant to overfitting.
- **Cluster-based Local Outlier Factor (CBLOF)**: CBLOF identifies outliers by considering their deviation from cluster centroids. It calculates the local outlier factor based on the density of neighboring clusters, making it effective for clustered datasets.
- **Histogram-based Outlier Detection (HBOS)**: HBOS divides feature space into bins and computes the anomaly score based on bin frequencies. It's computationally efficient but assumes feature independence and is sensitive to data distribution.
- **K Nearest Neighbors (KNN)**: KNN identifies outliers based on the distance to their k-nearest neighbors. It's simple, robust, and effective for high-dimensional data but sensitive to choice of k and distance metrics.
- **Principal Component Analysis (PCA)**: PCA transforms data into orthogonal components to identify outliers as data points with large reconstruction errors. It's useful for dimensionality reduction but may not capture nonlinear relationships.
- **One-Class Support Vector Machine (SVM)**: One-Class SVM learns a boundary around normal data points, identifying outliers as those lying outside this boundary. It's effective for high-dimensional data but sensitive to choice of kernel and parameter tuning.

<img src=images/readme-2d-n2000.png width='1200'>

## Open Source building data
The data used for time series analysis is from the US Department of Energy and Lawrence Berkeley National Laboratory. The [multi-year open source dataset](https://data.openei.org/submissions/5680) encompasses many different building time series such as electricity consumption, air flow, and room temperature. For this study, per minute CO2 parts per million readings and miscellaneous electrical consumption (kWh) every 15 minutes were used. Within the multi-year data 3 month periods were looked at to reduce the computational burden. 

<img src=images/description-co2-1.png width='700' style="margin-left: 20px;">
<img src=images/description-co2-2.png width='700' style="margin-left: 20px;">
<img src=images/description-ele-1.png width='700' style="margin-left: 20px;">
<img src=images/description-ele-2.png width='700' style="margin-left: 20px;">

## Unsupervised Anomaly Detection in building data
Despite their differences, these approaches underscore the importance of statistical analysis in anomaly detection, offering valuable insights into irregularities within building data without the need for prior knowledge or supervision. The following plots show the anomalous points each algorithm selected. 

- **Z-score** outlier detection in building time series involves calculating the standard deviation from the mean for each data point. Points deviating significantly from the mean, beyond a certain threshold (usually defined as a multiple of the standard deviation), are flagged as outliers. In building data, this method can identify anomalies such as extreme energy consumption or temperature fluctuations.
- **Isolation Forest** partitions building time series data using random trees, isolating outliers in sparser partitions. Outliers require fewer partitions to isolate, thus having shorter paths in the tree structure. This method efficiently handles high-dimensional data and is robust to outliers, making it suitable for detecting anomalies like abnormal equipment behavior or sudden system failures.
- **Local Outlier Factor** calculates the local density deviation of a data point with respect to its neighbors, identifying points with significantly lower densities as outliers. In building time series, this method can detect anomalies such as unusual patterns in energy consumption or unexpected changes in occupancy levels, providing insights into potential maintenance issues or operational irregularities.

<img src=images/unsupervised-ele_1.png width='700' style="margin-left: 20px;">
<img src=images/unsupervised-ele_2.png width='700' style="margin-left: 20px;">
<img src=images/unsupervised-ele_3.png width='700' style="margin-left: 20px;">
<img src=images/unsupervised-mels_S_1.png width='700' style="margin-left: 20px;">
<img src=images/unsupervised-mels_S_2.png width='700' style="margin-left: 20px;">
<img src=images/unsupervised-mels_S_3.png width='700' style="margin-left: 20px;">

## Supervised Anomaly Detection in building data
For supervised anomaly detection, there are two steps to take prior to training. The first is to batch time steps into N-steps. The batched time steps then predict the next time step value. The principle used for this type of anomaly detection measures the difference between the predicted value and the real value. If the difference is greater than some threshold the value is considered anomalous. Different techniques need to be used as anomalies become sparer in the training data but this simple technique can be efficacious for time series analysis. Secondly, hyperparameter tuning was undertaken with the models using Python `optuna` module so only the best performing models would be compared. 

Gradient Boosted Trees (GBT) and Long Short-Term Memory (LSTM) were chosen for the supervised anomaly detection example because of their popularity. GBT sequentially trains a multitude of decision trees, with each tree aiming to correct the errors made by its predecessors. In the context of anomaly detection, GBT can capture complex relationships and non-linear patterns within building time series data, enabling accurate predictions of normal behavior while flagging deviations as anomalies. On the other hand, LSTM, a type of recurrent neural network (RNN), excels in capturing temporal dependencies and long-term patterns in sequential data. By leveraging a network of interconnected memory cells, LSTM can effectively model the dynamic nature of building systems, detecting anomalies by identifying discrepancies between observed and forecasted values.

Final predictions are taken in periods of a week to reduce the computational burden. The results indicate that GBTs are a better option for anomaly detection in building data because it is a more accurate prediction method in general. Therefore, when outliers are identified, it can be determined with more confidence the error is due to an anomaly in the buildings behvaiour rather than an inaccuracy in the prediction. In the following charts, black represents the predicted values, green the historical value and red points the anomalies identified by the model.  

<img src=images/supervised_ele-1_mels_S.png width='800' style="margin-left: 20px;">
<img src=images/supervised_ele-2_mels_S.png width='800' style="margin-left: 20px;">
<img src=images/supervised_co2-1_zone_052_co2.png width='800' style="margin-left: 20px;">
<img src=images/supervised_co2-2_zone_072_co2.png width='800' style="margin-left: 20px;">

